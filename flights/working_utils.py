import kaggle
import numpy as np
import pandas as pd

def download_kaggle_data(filepath, competition='usdot/flight-delays'):
    try:
        kaggle.api.authenticate()
    except Exception as e:
        print("Config failed, check kaggle.json is configured correctly.\nDetails:\n")
        print(e)
        raise Exception
    try:
        kaggle.api.dataset_download_files(competition, path=filepath, unzip=True, force=True, quiet=False)
        print("Dataset successfully downloaded.")
    except Exception as e:
        print("Download failed, please retry.\nDetails:\n")
        print(e)
        

def load_data(path):
    files = list(path.iterdir())
    for file in files:
        print(file)
    airports = pd.read_csv(files[1])
    keep_cols = ['MONTH', 'DAY', 'DAY_OF_WEEK', 'AIRLINE', 'FLIGHT_NUMBER',
       'TAIL_NUMBER', 'ORIGIN_AIRPORT', 'DESTINATION_AIRPORT',
       'SCHEDULED_DEPARTURE', 'DEPARTURE_DELAY', 'TAXI_OUT', 'WHEELS_OFF',
       'SCHEDULED_TIME', 'DISTANCE','SCHEDULED_ARRIVAL', 'ARRIVAL_DELAY',
             'DIVERTED', 'CANCELLED',]
    data = pd.read_csv(files[2], usecols=keep_cols)
    data = data[data[['CANCELLED','DIVERTED']].sum(axis=1)==0].drop(['CANCELLED','DIVERTED'],axis=1).reset_index(drop=True)
    data['hours'], data['mins'] = data.SCHEDULED_DEPARTURE.astype(str).str.zfill(4).apply(lambda x: "{},{}".format(x[:2], x[2:])).str.split(',', 1).str
    return files, data, airports
    